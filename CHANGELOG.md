# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2024-02-12)


### Bug Fixes

* **husky:** fix husky hooks ([a218895](https://gitlab.com/gabrielmn/azure-functions-template/commit/a2188955ff131d517393db57ca31e806b9700844))
