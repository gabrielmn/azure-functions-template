# Azure Functions Node Template

This project is designed as a template for building serverless applications using Azure Functions with Node.js. It leverages TypeScript for strong typing and better development experience. The template is pre-configured with Jest for unit testing, Husky for managing Git hooks, and Commitlint to ensure commit message consistency.

## Features

- **Azure Functions**: Utilize the fully managed, scalable serverless compute platform for running event-triggered code without having to manage infrastructure.
- **TypeScript**: Benefit from static type checking, easier code refactoring, and improved readability with TypeScript.
- **Jest**: Write and execute unit tests with Jest to ensure your code behaves as expected.
- **Husky**: Use Git hooks to automate tasks like code formatting and linting before commits, ensuring only high-quality code is committed.
- **Commitlint**: Enforce a consistent commit message format, making the version control history clean and easy to navigate.

## Prerequisites

Before you start, ensure you have the following installed:
- Node.js (LTS version recommended)
- Azure Functions Core Tools
- Git

## Getting Started

To get started with this template, you have two options: running the project locally using Node.js or using Docker and Docker Compose for containerization. Follow the steps below for your preferred setup.

### Local

1. Clone the repository:
```bash
git clone git@gitlab.com:gabrielmn/azure-functions-template.git
 ```

2. Navigate to the project directory:
```bash
cd azure-functions-template
```

3. Install dependencies:
```bash
npm install
```

4. Start the local development server:
```bash
npm start
```
This command will compile TypeScript files, start the Azure Functions runtime, and listen for requests.

### Using Docker and Docker Compose

To containerize and run your application using Docker Compose, follow these simplified steps:

1. Start the project with Docker Compose:
```bash
docker-compose up --build
```

This single command builds the Docker image and starts the application by reading the **docker-compose.yml** file in your project directory. The **--build** option ensures that the Docker image is built (or rebuilt if necessary) before starting the containers.

Ensure your project directory includes a **Dockerfile** with the necessary setup for a Node.js environment and an Azure Functions runtime, as well as a **docker-compose.yml** file that defines how to build and run your containerized application.

By using **docker-compose up --build**, you streamline the development and deployment process, making it easier for users to get started with your template without having to run separate commands for building the Docker image and starting the Docker container.

## Running Tests
To run the unit tests with Jest, execute:
```bash
npm test
```
This command will find and run all tests in the project, displaying a summary of the results.

## Committing Changes

Before committing your changes, ensure they meet the commit message format enforced by Commitlint and pass all tests. Husky is configured to run tests and lint commit messages pre-commit. If any checks fail, the commit will be aborted. The commit message format can be found in the CONTRIBUTING file.


## Contributing

Contributions to this template are welcome. Please ensure your code passes all tests and follows the commit message guidelines before submitting a pull request.

## License

Specify the license under which this template is provided. Common choices include MIT, GPL, and Apache 2.0.