import { InvocationContext } from '@azure/functions';
import { httpTrigger1 } from '../src/functions/helloWorld'; // Adjust the import path based on your project structure

describe('httpTrigger1 Function', () => {
  it('should respond with the correct greeting', async () => {
    // Mock HttpRequest and Context
    const request = {
      query: new Map([['name', 'Azure']]),
      text: async () => '', // Mock text function if needed
    } as any; // Cast to 'any' to simplify mocking; consider a more precise type for production code

    const context = {
      log: jest.fn(), // Mock logging function
    } as unknown as InvocationContext;

    // Invoke the function
    const response = await httpTrigger1(request, context);

    // Assertions
    expect(response).toBeDefined();
    expect(response.body).toContain('Hello, Azure');
  });

  it('should use "world" if no name is provided', async () => {
    // Mock HttpRequest and Context without a name query parameter
    const request = {
      query: new Map(),
      text: async () => '', // Assume no body text is provided
    } as any;

    const context = {
      log: jest.fn(),
    } as unknown as InvocationContext;

    // Invoke the function
    const response = await httpTrigger1(request, context);

    // Assertions
    expect(response).toBeDefined();
    expect(response.body).toContain('Hello, world');
  });
});
